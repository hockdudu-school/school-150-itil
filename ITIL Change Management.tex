\documentclass[a4paper,10pt,titlepage]{article}

%%%%% START LANGUAGE %%%%%

\usepackage{polyglossia}
\usepackage[autostyle,german=swiss]{csquotes}
\setdefaultlanguage{german}
\setotherlanguage{english}

%%%%% END LANGUAGE %%%%%

%%%%% START TITLE %%%%%

\title{ITIL Change Management}
\author{Eduardo Rocha und Flynn Gassmann}

%%%%% END TITLE %%%%%

%%%%% START GRAPHICS %%%%%

\usepackage{graphicx}

%%%%% END GRAPHICS %%%%%

%%%%% START HYPERLINKS %%%%%

\usepackage{hyperref}
\hypersetup{colorlinks,%
citecolor=black,%
filecolor=black,%
linkcolor=black,%
urlcolor=black,
unicode=true}

%%%%% END HYPERLINKS %%%%%

\begin{document}

\maketitle

\begin{abstract}

Das Change Management ist ein sehr wichtiger Prozess für jegliche Änderung in der IT-Bereich. Es hilft das Planen von den Änderungen und minimiert deren Risiken. Bei jegliche Änderung folgt man mit dem Change Management immer denselben Ablauf. Dieser Ablauf enthält sehr wichtige Schritte, wie die Klassifikation und die Priorisierung von Anpassungen sowie die Analyse der Änderung nach der Implementierung.

\end{abstract}

\tableofcontents
\newpage

% Aufgabe 1: Änderungs-Prozess modellieren mit einem BPMN Hilfsmittel und dokumentieren (Wer, wann was). Es geht dabei auch um die formelle Freigabe einer Änderung und die Synchronisation mit anderen Änderungen um ICT Betrieb.

% Aufgabe 2: Vorgehen definieren mit dem die [sic] Auswirkung einer Änderung auf die verschiedenen Komponenten einer E-Business-Applikation oder einer WEB Anwendung abgeschätzt werden kann.

\section{Change Management} 

Oftmals geschieht es, dass Änderungen in den IT-Bereich teuer sind -- sie werden schlecht strukturiert und koordiniert. Sie brauchen mehr Zeit und mehr Ressourcen, die zwei Hauptursachen von Kosten bei Änderungen.

Change Management definiert Prozesse, Abläufe, die man folgt, um Veränderungen besser koordinieren zu können und damit die Kosten von jegliche Änderung zu reduzieren.

Das Change Management ist verantwortlich, den Change-Prozess zu erstellen und zu verwalten. Dieser Prozess enthält Dokumentationen, Genehmigung und Überwachung und stellt sicher, dass Änderungen günstig, effizient und mit minimalem Risiko ausgeführt werden können.

Bei dem Change-Prozess ist die Kommunikation der Schlüssel für einen erfolgreichen Ablauf. Ohne genug Kommunikation werden Änderungen nicht effizient durchgeführt und das kann dazu führen, dass Incidents -- Ereignisse, die nicht zum standardmässigen Betrieb eines Service gehören -- auftreten.

Der Prozess des Change Managements ist ein Vorschlag, wie man die Änderungen durchführen könnte. Firmen haben die Freiheit, Teile des Prozesses anzupassen.

\section{Ablauf von Änderungen}

\subsection{Überblick}

Jede Änderung folgt nach dem Change Management strikt denselben Prozess. Es ist von grosser Wichtigkeit, dass dieser Prozess immer gefolgt wird, unabhängig davon, ob die Änderung gross oder klein ist oder ob sie eine hohe oder niedrige Priorität hat. Nur wenn jede einzelne Änderung dem Change Management entsprechend bearbeitet wird, kann man den Überblick halten und dadurch die Änderungen besser planen.

Mit Änderung versteht man nicht nur eine Veränderung, ein Update eines Softwares, sondern auch Neuerungen, wie z.~B. eine neue Software, ein neues Produkt, und Korrekturen, also die Behebung eines Problems.

Der Ablauf einer Änderung kann in der Abbildung~\ref{diagram} angeschaut werden.

\begin{figure}
\includegraphics[angle=90,width=\textwidth]{diagram/diagram.png}
\caption{Änderungsprozess-Diagramm \label{diagram}}
\end{figure}

\subsection{Schritte}

Der Ablauf von den Änderungen folgt einen einfachen aber umfangreichen Prozess. Um diesen Prozess besser zu verstehen, kann man ihn in Schritte teilen. Der Anzahl Schritte hängt ab, von was man als einen Schritt definiert. Verschiedene Quellen geben eine verschiedene Anzahl Schritte, jedoch bleibt das Prozess dasselbe.

\subsubsection{Änderungsantrag}

Alle Änderungen beginnen mit einem Änderungsantrag (RfC -- Request for Change). Auf diesem Antrag wird logischerweise die gewünschte Änderung beschrieben. Neben der Änderung werden alle Aktivitäten des Ablaufs auf diesem Antrag dokumentiert, wie z.~B. Analysen, Anforderungen, Entscheidungen, Dokumentationen und Diskussionen.

\subsubsection{Registrierung und Klassifizierung}

Nachdem der Änderungsantrag geschrieben worden ist, wird dieser priorisiert und klassifiziert. Dieser Schritt ist wichtig, wenn es vielen Änderungsanträge gibt: Man kann nicht alles auf einmal machen. Mit der Priorisierung entscheidet man, was wichtiger ist und als erstes gemacht werden soll. Die Klassifizierung hilft der Schätzung der Änderung und der Weiterleitung des Antrages an der richtigen Abteilung (eine Änderung in der Hardware wird an einer anderen Ableitung als eine Anpassung in der Benutzeroberfläche weitergeleitet).

\subsubsection{Genehmigung}

Nicht alle Änderungsanträge werden durchgeführt. Darum werden diese vor der Ausarbeitung genehmigt. Es wird überprüft, ob diese Änderung überhaupt machbar ist. Es könnte ja sein, dass eine Änderung zu teuer zu implementieren ist -- braucht zu vielen Ressourcen oder zu viel Zeit -- und dadurch nicht durchgeführt werden kann, oder dass sie den Zielen der Firma nicht entspricht.

\subsubsection{Ausarbeitung und Tests}

Nachdem eine Änderung genehmigt worden ist, wird diese zur Ausarbeitung an die entsprechenden Gruppen weitergegeben. Das Change Management übernimmt die Koordination, um sicherzustellen, dass die Aktivitäten die erforderlichen Ressourcen bekommen und in den vorgegebenen Zeitplan durchgeführt werden kann. In dieser Schritt werden normalerweise auch Tests und Back-Out-Pläne erstellt.

\subsubsection{Freigabe an der Implementation}

Nachdem alles ausgearbeitet worden ist und die Tests und Back-Out-Pläne durchgeführt worden sind, wird die Änderung zur Implementation freigegeben.

\subsubsection{Implementierung}

Bei der Implementation hat Change Management die Aufgabe, dafür zu sorgen, dass die Änderungen in die vorgelegten Zeitrahmen ausgeführt werden können.

\subsubsection{Auswertung}

Änderungen werden nach der Implementation ausgewertet. Das geschieht durch einen \enquote{Post Implementation Review} (PIR). Der PIR hat das Ziel, die Effizienz der Implementation zu analysieren. Mit einer Soll-Ist-Analyse werden die Änderung und die dazu benutzten Methoden und Prozesse kontrolliert. Bei grossen Änderungen wird auch ein Kosten-Nutzen-Vergleich erstellt. Mit der Analyse kann man z.~B. überprüfen, welche Methode effizienter sind sowie welche Stolpersteine oft vorkommen und wie diese gelöst werden können.

\subsection{Rollen}

Das Change Management wird von verschiedene Personen mit unterschiedlichen Rollen ausgeführt. Hauptsächlich gibt es vier Rollen. Eine Person kann mehr als eine Rolle haben -- das ist der Fall z.~B. bei kleinen Unternehmen.

\subsubsection{Change Owner}

Der Change Owner (Änderungsaussteller) ist der Anforderer der Änderung. Er ist die Person, die den Änderungsantrag schreibt und diesen an den Projektmanager weitergibt.

\subsubsection{Projektmanager}

Der Projektmanager ist die Person, die für die Planung und Koordination aller Ressourcen in seinem Projekt verantwortlich ist. Er priorisiert und kategorisiert den Änderungsantrag und kann ihn entweder dem Change Manager weitergeleitet oder ihn ablehnen, wenn die Änderung nicht machbar ist.

\subsubsection{Change Manager}

Der Change Manager autorisiert, dokumentiert und steuert die Änderungen, nachdem ihm den Änderungsantrag weitergeleitet worden ist. Er analysiert den Änderungsantrag und, wenn er gültig und machbar ist, leitet ihn dem Projektteam weiter. Falls der Antrag nicht gültig ist, gibt er dem Projektmanager zurück zur Bearbeitung. Der Change Manager ist der Letzte, der die Änderung genehmigt oder ablehnt.

\subsubsection{Projektteam}

Das Projektteam besteht aus die, die die Änderung implementieren. Zusammen mit dem Projektmanager planen sie die Änderung gemäss die Vorgaben des Change Managers. Sie erstellen dann die Tests für die Änderung und implementieren sie danach.

\section{Auswirkungen von Änderungen}

Änderungen können verschiedene Auswirkungen haben. Die möglichen Auswirkungen hängen immer von der Änderung, von dem Fall ab. Um sich besser vorstellen können, wie weit die Auswirkungen von offenbar kleine Änderungen ausbreiten, wird dies mit einem Beispiel erklärt.

\subsection{Beispielfall}

Als Beispiel wird ein Web-Shop vorgestellt. Bei diesem Shop kann man Bilder von den Produkten anschauen, wie bei fast jedem Web-Shop. Die Vorschau von Bilder funktioniert, wie es sollte, aber dieser Web-Shop hat etwas nicht, was andere Web-Shops haben: Videos. Damit dieser Web-Shop konkurrenzfähig bleibt, muss er Videos unterstützen.

Auf den ersten Blick scheint diese Anpassung klein zu sein, aber wer das denkt, irrt sich komplett. Als Erstes muss es ermöglicht werden, dass die Mitarbeiter des Shops Videos zu den Produkten hinzufügen können. Nachher müssen die Videos an der Website eingebettet werden. Sie braucht einen Video-Player, der das Design der Website entspricht und zusätzlich Mobilgeräte unterstützt. Weil Videos eine grosse Menge Speicherplatz und und eine hohe Übertagungsrate brauchen, muss der Server, welcher die Web-Plattform hostet, bereit sein, die Videos zu bieten. Ausserdem müssen die Videos noch auf kleinere Auflösungen konvertiert werden -- für Clients mit eine langsame Verbindung --, was ein schnelles Server braucht sowie viel Recherche, wie Videos optimal konvertiert werden können. Wenn der Web-Shop zusätzlich Mobile-Apps hat, müssen die Videos auch an diesen Apps funktionieren.

\subsection{Analyse und Risiken}

Was auf den ersten Blick nach etwas Kleines ausgesehen hat, ist in der Tat eine sehr aufwendige Sache. Damit Änderungen nicht mehr Aufwand treiben, als man geplant hat, sollen in der Analysephase alle Aspekte und Auswirkungen berücksichtigt werden. Somit werden Risiken minimiert.

Hätte man an der Analysephase die Auswirkungen nicht genau angeschaut, könnte unter anderem folgendes geschehen: Entweder wird die Änderung zu spät implementiert, weil zu wenig Ressourcen zugeteilt worden sind, und die Firma verliert Kunden; oder die Änderung wird früh implementiert, aber z.~B. der Server wurde nicht verbessert, was dazu führt, dass die Website langsam wird, und es ist dann nicht so leicht, der genauen Grund herauszufinden; oder die Änderung wird doch richtig und in einer guten Zeit implementiert, aber die Firma hat damit Geld verloren, weil das Budget überschritten worden ist, denn die Ressourcen sind nicht optimal zugeteilt worden.

\section{Schlusswort}

Das Change Management ist ein sehr hilfreicher Prozess und hilft Firmen, Änderungen besser zu planen und zu überwachen. Der Aufwand, Änderungen mit Change Management zu verwalten, ist zwar grösser, als wenn man es nicht verwenden würde, aber das Change Management lohnt sich auf längere Sicht, denn es minimiert die Risiken einer nicht gut geplanten Änderung.


\section{Quellen}
Alle Quellen stammen von Mai 2019.

\begin{itemize}
 \item \href{https://de.wikipedia.org/wiki/Change\_Management\_(ITIL)}{https://de.wikipedia.org/wiki/Change\_Management\_(ITIL)}%Almost everything
 
 \item \href{http://www.oe-files.de/files/talks/itil-print.pdf}{http://www.oe-files.de/files/talks/itil-print.pdf} % Change Management - Begleitung aller Änderungen
 
 \item \href{https://www.vtt.fi/inf/pdf/publications/2000/P416.pdf}{https://www.vtt.fi/inf/pdf/publications/2000/P416.pdf} %Maybe for some really tiny small inspiration
 % NEW: Page 104 onwards are really helpful
 
\item\href{https://technology.wv.gov/SiteCollectionDocuments/Project\%20Management\%20Templates/Change\%20Management\%20Process\%2003\%2022\%202012.pdf}{https://technology.wv.gov/SiteCollectionDocuments/\\Project\%20Management\%20Templates/\\Change\%20Management\%20Process\%2003\%2022\%202012.pdf}%Base info for roles

\item\href{https://www.itsmprocesses.com/Dokumente/ITIL\%202011\%20Glossar\%20Rollen.pdf}{https://www.itsmprocesses.com/Dokumente/\\ITIL\%202011\%20Glossar\%20Rollen.pdf}%Translations

\end{itemize}
\end{document}
